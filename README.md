# TootBot

A small python script to replicate tweets on a mastodon account.
This is a fork of [cquest's tootbot](https://github.com/cquest/tootbot) that
uses Twitter's API instead of using RSS scrapers.

## Features

- Twitter tracking links (t.co) are dereferenced
- Twitter-hosted pictures, GIFs and videos are retrieved and uploaded to
  Mastodon if they are less than 5 MB big. If there are several variants of a
  video, the script picks the best-quality variant that is smaller than 5 MB. If
  all variants are too big, the video is not uploaded and a link to the tweet is
  left instead.
- Twitter users' handles are replaced with their long names
- Retweets are signaled with a bird emoji! 🐦

An sqlite database is used to keep track of tweets than have been tooted.

## Usage

Simply download the latest stable version (`master` branch of this Git
repository). The command to run is:

```
python tootbot.py <twitter_handle> <mastodon_email> <mastodon_password> <mastodon_instance>
```
where `<twitter_handle>` is without `@`, and `<mastodon_email>` is the email
used to create the Mastodon destination account. This command will repost the
5 latest tweets of the Twitter account to Mastodon. It is your responsibility to
run the script periodically if you want to make a reposting bot, for instance
via a cron job.

The script needs Mastodon login/pass to post toots, as well as a set of Twitter
application credentials (see
https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens).


The simplest way to use the script is to simply make it run as a cron job on any
server (not necessarily the Mastodon instance server).
