import os
import os.path
import sys
import twitter
from mastodon import Mastodon
import json
import requests
import re
import sqlite3
from datetime import datetime, date, time, timedelta

if len(sys.argv) < 4:
    print("Usage: python3 tootbot.py twitter_account mastodon_login mastodon_passwd mastodon_instance")
    sys.exit(1)

# If lock file is present, exit, otherwise create it
if os.path.isfile('lockfile'):
    sys.exit(0)
open('lockfile', 'w+').close()

try:
    # sqlite db to store processed tweets (and corresponding toots ids)
    sql = sqlite3.connect('tootbot.db')
    db = sql.cursor()
    db.execute('''CREATE TABLE IF NOT EXISTS tweets (tweet text, toot text, twitter text, mastodon text, instance text)''')

    if len(sys.argv)>4:
        instance = sys.argv[4]
    else:
        instance = 'amicale.net'

    if len(sys.argv)>5:
        days = int(sys.argv[5])
    else:
        days = 1

    twittername = sys.argv[1]
    mastodon = sys.argv[2]
    passwd = sys.argv[3]

    mastodon_api = None

    # TWITTER APP CREDENTIALS
    CONSUMER_KEY = None
    CONSUMER_SECRET = None
    ACCESS_TOKEN = None
    ACCESS_TOKEN_SECRET = None

    api = twitter.Api(consumer_key = CONSUMER_KEY, consumer_secret = CONSUMER_SECRET, access_token_key = ACCESS_TOKEN, access_token_secret = ACCESS_TOKEN_SECRET, tweet_mode = 'extended')
    # Fetch max. 50 statuses
    feed = api.GetUserTimeline(screen_name = twittername, exclude_replies = True,
            count = 5)

    for t in reversed(feed):
        # check if this tweet has been processed
        db.execute('SELECT * FROM tweets WHERE tweet = ? AND twitter = ?  and mastodon = ? and instance = ?',(t.id, twittername, mastodon, instance))
        last = db.fetchone()

        # process only unprocessed tweets less than 1 day old
        if last is None and (datetime.now()-datetime.strptime(t.created_at, "%a %b %d %H:%M:%S +0000 %Y") < timedelta(days=days)):
            if mastodon_api is None:
                # Create application if it does not exist
                if not os.path.isfile(instance+'.secret'):
                    if Mastodon.create_app(
                        'tootbot',
                        api_base_url='https://'+instance,
                        to_file = instance+'.secret'
                    ):
                        print('tootbot app created on instance '+instance)
                    else:
                        print('failed to create app on instance '+instance)
                        sys.exit(1)

                try:
                    mastodon_api = Mastodon(
                      client_id=instance+'.secret',
                      api_base_url='https://'+instance
                    )
                    mastodon_api.log_in(
                        username=mastodon,
                        password=passwd,
                        scopes=['read', 'write'],
                        to_file=mastodon+".secret"
                    )
                except:
                    print("ERROR: First Login Failed!")
                    sys.exit(1)

            mastodon_handle = mastodon_api.account_verify_credentials()['acct']

            #h = BeautifulSoup(t.summary_detail.value, "html.parser")
            if t.retweeted_status is not None:
                c = t.retweeted_status.full_text
                c = (u"\U0001f426 RT de %s\xa0:\n" % t.retweeted_status.user.name) + c
                # From now on, work with the retweeted status rather than the
                # main object (e.g. the main object doesn't contain media)
                tweet_media = t.retweeted_status.media
            else:
                c = t.full_text
                tweet_media = t.media

            toot_media = []
            remove_media_links = True
            # get the media...
            if tweet_media is not None:
                for m in tweet_media:
                    if m.type == "photo":
                        media = requests.get(m.media_url)
                        media_posted = mastodon_api.media_post(media.content, mime_type=media.headers.get('content-type'))
                        toot_media.append(media_posted['id'])
                    elif m.type == "animated_gif":
                        media = requests.get(m.media_url)
                        # Generally avoid media bigger than 8 MB
                        if media.headers.get('content-length') < 7800000:
                            media_posted = mastodon_api.media_post(media.content, mime_type=media.headers.get('content-type'))
                            toot_media.append(media_posted['id'])
                    elif m.type == "video" and m.video_info is not None:
                        #print('Attempting to repost video.')
                        d = m.video_info['duration_millis'] / 1000.0
                        # Filter out all videos bigger than about 5 MB
                        v = filter(lambda x: x['bitrate'] * d / 8 < 5000000 if 'bitrate' in x else False, m.video_info['variants'])
                        try:
                            # May fail if v is empty
                            best_variant = max(v, key=lambda x: x['bitrate'] if 'bitrate' in x else -1)
                            #print("Best variant size: ", best_variant['bitrate'] * d / 8)
                            #print("Downloading video bitrate: ", best_variant['bitrate'])
                            #print("Downloading video at: ", best_variant['url'])
                            media = requests.get(best_variant['url'])
                            #print("Posting to instance...")
                            media_posted = mastodon_api.media_post(media.content, mime_type=media.headers.get('content-type'))
                            #print("Posted.")
                            toot_media.append(media_posted['id'])
                        except ValueError:
                            # If no variant of the video is small enough, leave Twitter video link
                            remove_media_links = False

            # replace t.co link by original URL
            matches = re.finditer(r"https?://[^ \n\xa0]*", c)
            length = len(c)
            try:
                for m in matches:
                    l = m.group(0)
                    r = requests.get(l, allow_redirects=False)
                    if r.status_code in {301,302}:
                        url = r.headers.get('Location')
                        # Replace by original URL only if it does not result in the toot
                        # exceeding 500 characters.
                        new_length = length - len(l) + len(url)
                        if new_length < 500:
                            c = c.replace(l,r.headers.get('Location'))
                            length = new_length
            except UnicodeDecodeError:
                # This has happenned lately when the tweet contained some
                # non-Unicode character. Let's just drop the tweet, I don't
                # have much time to spend on that bot.
                continue

            if remove_media_links:
                # remove pic.twitter.com links
                m = re.search(r"pic.twitter.com[^ \xa0\n]*", c)
                if m != None:
                    l = m.group(0)
                    c = c.replace(l,' ')
                # remove Twitter photo and video links
                m = re.search(r"https?://twitter.com/[^ \xa0\n]*?/(photo|video)/[0-9]", c)
                if m != None:
                    l = m.group(0)
                    c = c.replace(l,' ')

            length = len(c)
            # replace mentioned users' Twitter handles with their long names
            for user in t._json['entities']['user_mentions']:
                new_length = length - len(user['screen_name']) + len(user['name'])
                if new_length < 500:
                    c = c.replace('@'+user['screen_name'], user['name'], 1)
                    length = new_length
            # replace self-mentions by the right Mastodon handle
            matches = re.finditer(r"@"+twittername, c, flags=re.I)
            for m in matches:
                l = m.group(0)
                new_length = length - len(l) + len("@" + mastodon_handle)
                if new_length < 500:
                    c = c.replace(l, '@'+mastodon_handle, 1)
                    length = new_length

            #Clean up the way quoted tweets are shown
            if t.quoted_status is not None:
                link = 'https://twitter.com/'+t.quoted_status.user.screen_name+'/status/'+str(t.quoted_status.id)
                if t.quoted_status.user.screen_name == t.user.screen_name:
                    quote='\n\nEn commentant son propre tweet:\n'
                else:
                    quote='\n\nEn commentant le tweet de '+t.quoted_status.user.name+':\n'
                if length + len(quote) < 500:
                    c = c.replace(link,'')
                    c = c + quote+link

            toot = mastodon_api.status_post(c, in_reply_to_id=None, media_ids=toot_media, sensitive=False, visibility='public', spoiler_text=None)
            if "id" in toot:
                db.execute("INSERT INTO tweets VALUES ( ? , ? , ? , ? , ? )",
                (t.id, toot["id"], twittername, mastodon, instance))
                sql.commit()
except:
    # Remove lock file
    os.remove('lockfile')
    raise

# Remove lock file
os.remove('lockfile')
